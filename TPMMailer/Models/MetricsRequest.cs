﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPMMailerWorker.Models
{
    public class MetricsRequest
    {
        public string ProfileID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public AggregationLevel Aggregation { get; set; }
        public List<ProviderMetric> Metrics { get; set; }
    }

    public enum AggregationLevel
    {
        None,
        Yearly,
        Monthly,
        Daily
    }

    public class ProviderMetric
    {
        public string Provider { get; set; }
        public List<string> Metrics { get; set; }
    }
}
