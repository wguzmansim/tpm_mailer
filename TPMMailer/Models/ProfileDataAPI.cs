﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPMMailerWorker.Models.APIResponse
{
    public class ProfileDataAPI
    {
        public string ProfileImage { get; set; }
        public string DirectoryLogo { get; set; }
        public string BusinessName { get; set; }
        public string AddressLine1 { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public dynamic CustomFields { get; set; }
    }
}