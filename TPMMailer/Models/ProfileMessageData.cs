﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using TPMMailerWorker.Models.APIResponse;

using Newtonsoft.Json;
using TPMLog;
using System.Configuration;
using System.Globalization;

namespace TPMMailerWorker.Models
{
    public class ProfileMessageData
    {
        public Profile Profile { get; set; }
        public List<ThirdPartyMetric> ThirdPartyMetrics { get; set; }

        public ProfileMessageData CreateMessageObject(ProfileDataAPI ProfileData, ProfileMetricsData ProfileMetricsData, string DirectoryID)
        {
            Log.LogMessage(string.Format("Starting HTML object for Profile ID {1}, Directory ID {0}", DirectoryID, ProfileMetricsData.Profile.ID), ConfigurationManager.AppSettings["LogFileName"]);

            var FinalObject = new ProfileMessageData();
            var FinalObjectProfile = new Profile();
            try
            {
                var FinalObjectThirdPartyMetric = new ThirdPartyMetric();

                FinalObjectProfile.ID = ProfileMetricsData.Profile.ID;
                FinalObjectProfile.ProfileLogo = ProfileMetricsData.Profile.ProfileImage == string.Empty ? null : ProfileMetricsData.Profile.ProfileImage;
                FinalObjectProfile.AddressLine1 = ProfileMetricsData.Profile.AddressLine1;
                FinalObjectProfile.DirectoryLogo = ProfileMetricsData.Profile.DirectoryImage == string.Empty ? null : ProfileMetricsData.Profile.DirectoryImage;
                FinalObjectProfile.DirectoryID = DirectoryID;
                FinalObjectProfile.BusinessName = ProfileMetricsData.Profile.BusinessName;
                FinalObjectProfile.Name = ProfileMetricsData.Profile.Name;
                FinalObjectProfile.ZipCode = ProfileMetricsData.Profile.ZipCode;
                FinalObjectProfile.Phone = FormatPhone(ProfileMetricsData.Profile.Phone);
                FinalObjectProfile.City = ProfileMetricsData.Profile.City;
                FinalObjectProfile.State = ProfileMetricsData.Profile.State;
                FinalObjectProfile.CustomFields = ProfileData.CustomFields;
                FinalObject.Profile = FinalObjectProfile;

                foreach (var ThirdPartyMetricData in ProfileMetricsData.ThirdPartyMetrics)
                {
                    var ThirdPartyMetricsItem = new ThirdPartyMetric()
                    {
                        ThirdPartyProviderID = ThirdPartyMetricData.ThirdPartyProviderID,
                        ThirdPartyName = ThirdPartyMetricData.ThirdPartyName,
                        StartDate = new StartDate()
                        {
                            Year = ThirdPartyMetricData.StartDate.Year,
                            Month = ThirdPartyMetricData.StartDate.ToString("MMM", CultureInfo.InvariantCulture),
                            Day = ThirdPartyMetricData.StartDate.Day
                        },
                        EndDate = new EndDate()
                        {
                            Year = ThirdPartyMetricData.EndDate.Year,
                            Month = ThirdPartyMetricData.EndDate.ToString("MMM", CultureInfo.InvariantCulture),
                            Day = ThirdPartyMetricData.EndDate.Day
                        },
                        Aggregation = ThirdPartyMetricData.Aggregation
                    };

                    FinalObject.ThirdPartyMetrics = new List<ThirdPartyMetric>();
                    ThirdPartyMetricsItem.Metrics = new List<Metric>();

                    foreach (var MetricItem in ThirdPartyMetricData.Metrics)
                    {
                        var NewMetricItem = new Metric()
                        {
                            MetricID = MetricItem.MetricID,
                            MetricName = MetricItem.MetricName,
                            MetricFriendlyName = MetricItem.MetricFriendlyName,
                            MetricDescription = MetricItem.MetricDescription,
                            Summary = new Summary()
                            {
                                Average = MetricItem.Summary.Average,
                                Max = MetricItem.Summary.Max,
                                Min = MetricItem.Summary.Min
                            }
                        };
                        NewMetricItem.MetricValues = new List<MetricValue>();
                        foreach (var MetricValue in MetricItem.MetricValues)
                        {
                            var NewMetricValue = new MetricValue()
                            {
                                Date = MetricValue.Date,
                                Value = MetricValue.Value
                            };
                            NewMetricItem.MetricValues.Add(NewMetricValue);
                        }
                        ThirdPartyMetricsItem.Metrics.Add(NewMetricItem);
                    }

                    FinalObject.ThirdPartyMetrics.Add(ThirdPartyMetricsItem);
                }

                Log.LogMessage(string.Format("Finished HTML object for Profile ID {1}, Directory ID {0}", DirectoryID, ProfileMetricsData.Profile.ID), ConfigurationManager.AppSettings["LogFileName"]);
            }
            catch (Exception e)
            {
                Log.LogMessage(string.Format("Error creating HTML object for Profile ID {1}, Directory ID {0}, error = {2}", DirectoryID, ProfileMetricsData.Profile.ID, e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
            }
            
            return FinalObject;

        }

        public string FormatPhone(string Phone)
        {
            string FinalPhone = "";
            if (Phone.Length == 10)
            {
                FinalPhone = Regex.Replace(Phone, @"(\d{3})(\d{3})(\d{4})", "($1) $2-$3");
            }
            else
            {
                Phone = "+" + Phone.Substring(0, (Phone.Length - 10)) + String.Format("{0:(###) ###-####}", Phone);

            }
            return FinalPhone;
        }
    }
    public class Profile
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string DirectoryID { get; set; }
        public string DirectoryName { get; set; }
        public string BusinessName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string TollfreeNumber { get; set; }
        public string DirectoryLogo { get; set; }
        public string ProfileLogo { get; set; }
        public dynamic CustomFields { get; set; }
    }

    public class StartDate
    {
        public int Year { get; set; }
        public string Month { get; set; }
        public int Day { get; set; }
    }

    public class EndDate
    {
        public int Year { get; set; }
        public string Month { get; set; }
        public int Day { get; set; }
    }

    public class Summary
    {
        public double Average { get; set; }
        public double Max { get; set; }
        public double Min { get; set; }
    }

    public class MetricValue
    {
        public string Date { get; set; }
        public decimal Value { get; set; }
        public string DisplayValue {
            get
            {
                return string.Format("{0:#,0}", Value); ;
            }
        }
    }

    public class Metric
    {
        public string MetricID { get; set; }
        public string MetricName { get; set; }
        public string MetricFriendlyName { get; set; }
        public string MetricDescription { get; set; }
        public Summary Summary { get; set; }
        public List<MetricValue> MetricValues { get; set; }
    }

    public class ThirdPartyMetric
    {
        public string ThirdPartyProviderID { get; set; }
        public string ThirdPartyName { get; set; }
        public StartDate StartDate { get; set; }
        public EndDate EndDate { get; set; }
        public string Aggregation { get; set; }
        public List<Metric> Metrics { get; set; }
    }   
}
