﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;
using System.Net;

namespace TPMMailerWorker.Models.APIResponse
{
    public class ProfileMetricsData
    {
        public Profile Profile { get; set; }
        public List<ThirdPartyMetric> ThirdPartyMetrics { get; set; }
        public Status Status { get; set; }
    }
    public class Profile
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string DirectoryName { get; set; }
        public string BusinessName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string TollfreeNumber { get; set; }
        public string ProfileImage { get; set; }
        public string DirectoryImage { get; set; }
    }

    public class StartDate
    {
        public int Year { get; set; }
        public string Month { get; set; }
        public int Day { get; set; }
    }

    public class EndDate
    {
        public int Year { get; set; }
        public string Month { get; set; }
        public int Day { get; set; }
    }

    public class Summary
    {
        public double Average { get; set; }
        public double Max { get; set; }
        public double Min { get; set; }
    }

    public class MetricValue
    {
        public string Date { get; set; }
        public decimal Value { get; set; }
        public string DisplayValue
        {
            get
            {
                return string.Format("{0:#,0}", Value);
            }
        }


    }

    public class Metric
    {
        public string MetricID { get; set; }
        public string MetricName { get; set; }
        public string MetricFriendlyName { get; set; }
        public string MetricDescription { get; set; }
        public Summary Summary { get; set; }
        public List<MetricValue> MetricValues { get; set; }
    }

    public class ThirdPartyMetric
    {
        public string ThirdPartyProviderID { get; set; }
        public string ThirdPartyName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Aggregation { get; set; }
        public List<Metric> Metrics { get; set; }
    }

    public class Status
    {
        public bool Success { get; set; }
        public object Error { get; set; }
    }
}
