﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.IO;

using TPMMailerWorker.Models;
using TPMLog;
using System.Configuration;
using System.Diagnostics;
using System.Net;

namespace TPMMailerWorker
{
    class Program
    {
        static void Main(string[] args)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            //Debugger.Launch();
            try
            {
                ProfileEmailData ProfileEmailData = new ProfileEmailData();

                //Checks if arguments are passed
                if (args.Length == 0)
                {
                    Log.LogMessage(string.Format("No arguments passed for the TPMMailerWorker"), ConfigurationManager.AppSettings["LogFileErrorName"]);
                    return;
                }
                ProfileEmailData = JsonConvert.DeserializeObject<ProfileEmailData>(String.Join(" ", args));
                string LogMessage = "Starting email worker for Directory ID {0}, Profile ID {1}";
                Log.LogMessage(string.Format(LogMessage, ProfileEmailData.DirectoryID, ProfileEmailData.ProfileID), ConfigurationManager.AppSettings["LogFileName"]);

                TPMMailerWorker Worker = new TPMMailerWorker();
                Worker.SendHTMLEmail(ProfileEmailData);
            }
            catch (Exception e)
            {
                Log.LogMessage(string.Format("Error trying to start the TPMMailerWorker, error = {0}", e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                Environment.Exit(1);
            }
            
        }
    }
}
