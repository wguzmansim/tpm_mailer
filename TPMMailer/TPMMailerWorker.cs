﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPMMailerWorker.Models;

using Nustache;
using Nustache.Core;

using System.Net;
using System.Net.Mail;
using Newtonsoft.Json;
using TPMLog;
using TPMMailerWorker.Models.APIResponse;
using System.Globalization;

namespace TPMMailerWorker
{
    public class TPMMailerWorker
    {
        private string DatePathFolder = DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") + "\\" + DateTime.Now.ToString("dd");

        public void SendHTMLEmail(ProfileEmailData ProfileEmailData)
        {
            //Gets information from Profile API
            var ProfileData = new TPMProfileDataAPIWorker().GetProfileData(ProfileEmailData.ProfileID, ProfileEmailData.DirectoryID);

            //Checks if there is profile data
            if (ProfileData == null)
            {
                Log.LogMessage(string.Format("There are no profile data for Profile ID {1}, Directory ID {0}", ProfileEmailData.DirectoryID, ProfileEmailData.ProfileID), ConfigurationManager.AppSettings["LogFileErrorName"]);
                Environment.Exit(1);
            }

            //Saves ProfileData JSON object
            System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["ProfileDataFolder"] + "\\" + DatePathFolder);
            var DateFileSaved = DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss");
            using (StreamWriter file = File.CreateText(ConfigurationManager.AppSettings["ProfileDataFolder"] + "\\" + DatePathFolder + "\\Directory_" + ProfileEmailData.DirectoryID + "_Profile_" + ProfileEmailData.ProfileID + "_" + DateFileSaved + ".json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, ProfileData);
            };

            //Gets information from TPMMetrics API
            var ProfileMetricsData = new TPMProfileMetricsAPIWorker().GetMetricsData(ProfileEmailData.ProfileID, ProfileEmailData.DirectoryID);

            //Checks if there is profile metrics data
            if (ProfileMetricsData.Profile == null){ 
                Log.LogMessage(string.Format("There are no metrics data for Profile ID {1}, Directory ID {0}", ProfileEmailData.DirectoryID, ProfileEmailData.ProfileID), ConfigurationManager.AppSettings["LogFileErrorName"]);
                Environment.Exit(1);
            }

            //Saves ProfileMetricsData JSON object
            System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["ProfileMetricsDataFolder"] + "\\" + DatePathFolder);
            using (StreamWriter file = File.CreateText(ConfigurationManager.AppSettings["ProfileMetricsDataFolder"] + "\\" + DatePathFolder + "\\Directory_" + ProfileEmailData.DirectoryID + "_Profile_" + ProfileEmailData.ProfileID + "_" + DateFileSaved + ".json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, ProfileMetricsData);
            };

            //Creates object to be sent as message in the email
            var ProfileMessageData = new ProfileMessageData().CreateMessageObject(ProfileData, ProfileMetricsData, ProfileEmailData.DirectoryID);

            //Saves ProfileMetricsData JSON object
            System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["ProfileMessageDataFolder"] + "\\" + DatePathFolder);
            using (StreamWriter file = File.CreateText(ConfigurationManager.AppSettings["ProfileMessageDataFolder"] + "\\" + DatePathFolder + "\\Directory_" + ProfileEmailData.DirectoryID + "_Profile_" + ProfileEmailData.ProfileID + "_" + DateFileSaved + ".json"))
            {
                var decSer = new JsonSerializer();
                decSer.Converters.Add(new DecimalFormatConverter());
                decSer.Formatting = Formatting.Indented;
                decSer.NullValueHandling = NullValueHandling.Ignore;
                using (JsonWriter jw = new JsonTextWriter(file))
                {
                    decSer.Serialize(jw, ProfileMessageData);
                    file.Flush();
                }
            };


            //Gets HTML template
            Log.LogMessage(string.Format("Starting mustache creation for Profile ID {1}, Directory ID {0}", ProfileEmailData.DirectoryID, ProfileEmailData.ProfileID), ConfigurationManager.AppSettings["LogFileName"]);
            var MustacheMessage = Render.FileToString(string.Format(ConfigurationManager.AppSettings["EmailTemplateFilename"], ProfileEmailData.DirectoryID), ProfileMessageData);
            Log.LogMessage(string.Format("Finished mustache creation for Profile ID {1}, Directory ID {0}", ProfileEmailData.DirectoryID, ProfileEmailData.ProfileID), ConfigurationManager.AppSettings["LogFileName"]);

            //Merges information with template
            var FinalHTML = File.ReadAllText(ConfigurationManager.AppSettings["EmailTemplateHeader"]) + MustacheMessage + File.ReadAllText(ConfigurationManager.AppSettings["EmailTemplateFooter"]);

            //Saves HTML code
            System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["ProfileHTMLFolder"] + "\\" + DatePathFolder);
            TextWriter HTMLFile = new StreamWriter(ConfigurationManager.AppSettings["ProfileHTMLFolder"] + "\\" + DatePathFolder + "\\Directory_" + ProfileEmailData.DirectoryID + "_Profile_" + ProfileEmailData.ProfileID + "_" + DateFileSaved + ".html");
            HTMLFile.WriteLine(FinalHTML);
            HTMLFile.Close();

            //Starts sending email in HTML format
            Log.LogMessage(string.Format("Starts HTML email delivery for Profile ID {1}, Directory ID {0}", ProfileEmailData.DirectoryID, ProfileEmailData.ProfileID), ConfigurationManager.AppSettings["LogFileName"]);


            try
            {
                SendMail(ConfigurationManager.AppSettings["MailAddressFromEmail"], string.Join(",", ProfileEmailData.Email),
                    String.Format(ConfigurationManager.AppSettings["MailSubject"], Convert.ToDateTime(ConfigurationManager.AppSettings["MetricsStartDate"]).ToString("MMMM"), Convert.ToDateTime(ConfigurationManager.AppSettings["MetricsStartDate"]).ToString("yyyy")), FinalHTML);
            }
            catch (Exception e)
            {
                Log.LogMessage(string.Format("Error sending HTML email for Profile ID {1}, Directory ID {0}, error = {2}", ProfileEmailData.DirectoryID, ProfileEmailData.ProfileID, e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                Environment.Exit(1);
            }

            return;
        }

        public static void SendMail(string From, string To, string Subject, string Body)
        {
            var smtpNotification = new SmtpClient(ConfigurationManager.AppSettings["MailSMTPServer"]);
            var mail = new MailMessage(From, To)
            {
                IsBodyHtml = true,
                Body = Body,
                Subject = Subject
            };
            smtpNotification.Send(mail);
        }
    }

    public class DecimalFormatConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(decimal));
        }

        public override void WriteJson(JsonWriter writer, object value,
                                       JsonSerializer serializer)
        {
            writer.WriteValue(string.Format("{0:0,0}", value));
        }

        public override bool CanRead
        {
            get { return false; }
        }

        public override object ReadJson(JsonReader reader, Type objectType,
                                     object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
