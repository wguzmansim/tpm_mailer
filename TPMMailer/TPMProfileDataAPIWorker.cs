﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using TPMMailerWorker.Models;
using System.Configuration;

using TPMMailerWorker.Models.APIResponse;
using System.IO;
using TPMLog;

namespace TPMMailerWorker
{
    public class TPMProfileDataAPIWorker
    {
        private string VelocityTokenAPIURL = ConfigurationManager.AppSettings["VelocityTokenAPIURL"];
        private string VelocityAPIKey = ConfigurationManager.AppSettings["VelocityAPIKey"];
        private string ProfileDataAPIURL = ConfigurationManager.AppSettings["ProfileDataAPIURL"];
        private string ProfileDataAPIToken = ConfigurationManager.AppSettings["ProfileDataAPIToken"];

        public ProfileDataAPI GetProfileData(string ProfileID, string DirectoryID)
        {
            Log.LogMessage(string.Format("Starting getting profile data for Profile ID {1}, Directory ID {0}", DirectoryID, ProfileID), ConfigurationManager.AppSettings["LogFileName"]);
            ProfileDataAPI ProfileDataJson = new ProfileDataAPI();
            try
            {
                string ProfileDataAPITokenCalculated = ProfileDataAPIToken;

                WebClient Client = new WebClient();
                string ProfileData = Client.DownloadString(ProfileDataAPIURL + ProfileID + "?format=json&token=" + ProfileDataAPITokenCalculated);
                dynamic d = JObject.Parse(ProfileData);
                string Schema = d.Schema;
                string CleanSchema = Schema.Substring(0, Schema.Length - 9);
                CleanSchema = CleanSchema.Remove(0, 35);
                dynamic SchemaJson = JObject.Parse(CleanSchema);
                ProfileDataJson.ProfileImage = SchemaJson.image;
                ProfileDataJson.DirectoryLogo = SchemaJson.logo;
                ProfileDataJson.BusinessName = SchemaJson.name;

                ProfileDataJson.City = d.AddressAndGeo.City;
                ProfileDataJson.AddressLine1 = d.AddressAndGeo.AddressLine1;
                ProfileDataJson.State = d.AddressAndGeo.StateCode;
                ProfileDataJson.ZipCode = d.AddressAndGeo.ZipCode;


                string ProfileDataString = d.ProfileData;
                string CleanProfileData = ProfileDataString.Substring(0, ProfileDataString.Length - 10);
                CleanProfileData = CleanProfileData.Remove(0, 47);
                dynamic ProfileDataJsonClean = JObject.Parse(CleanProfileData);
                ProfileDataJson.Phone = ProfileDataJsonClean.phone.digits;

                try
                {
                    ProfileDataJson.CustomFields = d.CustomFields;
                }
                catch (Exception)
                {
                    //Profile doesn't have CustomFields... do nothing...
                }
            }
            catch (Exception e)
            {
                Log.LogMessage(string.Format("Error getting profile data for Profile ID {1}, Directory ID {0}, error = {2}", DirectoryID, ProfileID, e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                Environment.Exit(1);
            }



            Log.LogMessage(string.Format("Finished getting profile data for Profile ID {1}, Directory ID {0}", DirectoryID, ProfileID), ConfigurationManager.AppSettings["LogFileName"]);
            return ProfileDataJson;
        }

        private string GetToken()
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(VelocityTokenAPIURL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"ApiKey\":\"" + VelocityAPIKey + "\"}";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                var ProfileDataAPITokenResponse = JsonConvert.DeserializeObject<ProfileDataToken>(result);
                if (ProfileDataAPITokenResponse.Success)
                {
                    return ProfileDataAPITokenResponse.Token;
                }
                else
                {
                    return String.Empty;
                }
            }
            catch (Exception e)
            {
                Log.LogMessage(string.Format("Error getting token for ProfileData, error = {0}", e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                return String.Empty;
            }
        }
    }
}
