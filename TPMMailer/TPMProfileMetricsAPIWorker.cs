﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using TPMMailerWorker.Models;
using System.Configuration;
using System.IO;

using TPMMailerWorker.Models.APIResponse;
using TPMLog;
using TPMMailerWorker.Models;

namespace TPMMailerWorker
{
    public class TPMProfileMetricsAPIWorker
    {
        private string ProfileMetricsDataAPIURL = ConfigurationManager.AppSettings["ProfileMetricsDataAPIURL"];
        private string VelocityAPIKey = ConfigurationManager.AppSettings["VelocityAPIKey"];
        private string VelocityTokenAPIURL = ConfigurationManager.AppSettings["VelocityTokenAPIURL"];


        public ProfileMetricsData GetMetricsData(string ProfileID, string DirectoryID)
        {
            Log.LogMessage(string.Format("Starting getting profile metrics data for Profile ID {1}, Directory ID {0}", DirectoryID, ProfileID), ConfigurationManager.AppSettings["LogFileName"]);

            ProfileMetricsData MetricsData = new ProfileMetricsData();

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(ProfileMetricsDataAPIURL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers["token"] = GetToken();

                if (httpWebRequest.Headers["token"] == "")
                {
                    Log.LogMessage(string.Format("Error getting token for ProfileMetricsAPI for Profile ID {1}, Directory ID {0}", DirectoryID, ProfileID), ConfigurationManager.AppSettings["LogFileErrorName"]);
                    Environment.Exit(1);
                }

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var MetricsRequest = new Models.MetricsRequest();
                    MetricsRequest.Aggregation = (Models.AggregationLevel)Enum.Parse(typeof(Models.AggregationLevel), ConfigurationManager.AppSettings["MetricsAggregationLevel"]);
                    MetricsRequest.ProfileID = ProfileID;
                    MetricsRequest.StartDate = Convert.ToDateTime(ConfigurationManager.AppSettings["MetricsStartDate"]);
                    MetricsRequest.EndDate = Convert.ToDateTime(ConfigurationManager.AppSettings["MetricsEndDate"]);
                    MetricsRequest.Metrics = new List<Models.ProviderMetric>();
                    MetricsRequest.Metrics.Add(new Models.ProviderMetric
                    {
                        Provider = "GoogleMyBusiness",
                        Metrics = new List<string>() {
                            "GoogleMyBusiness_VIEWS_SEARCH",
                            "GoogleMyBusiness_ACTIONS_PHONE",
                            "GoogleMyBusiness_ACTIONS_DRIVING_DIRECTIONS",
                            "GoogleMyBusiness_ACTIONS_WEBSITE",
                            "GoogleMyBusiness_VIEWS_MAPS",
                            "GoogleMyBusiness_QUERIES_INDIRECT",
                            "GoogleMyBusiness_QUERIES_DIRECT"
                        }
                    });

                    streamWriter.Write(JsonConvert.SerializeObject(MetricsRequest));
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                MetricsData = JsonConvert.DeserializeObject<ProfileMetricsData>(result);

                Log.LogMessage(string.Format("Finished getting profile metrics data for Profile ID {1}, Directory ID {0}", DirectoryID, ProfileID), ConfigurationManager.AppSettings["LogFileName"]);
            }
            catch (Exception e)
            {
                Log.LogMessage(string.Format("Error getting profile metrics data for Profile ID {1}, Directory ID {0}, error = {2}", DirectoryID, ProfileID, e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                Environment.Exit(1);
            }

            return MetricsData;
        }
        private string GetToken()
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(VelocityTokenAPIURL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"ApiKey\":\"" + VelocityAPIKey + "\"}";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                var ProfileDataAPITokenResponse = JsonConvert.DeserializeObject<ProfileDataToken>(result);
                if (ProfileDataAPITokenResponse.Success)
                {
                    return ProfileDataAPITokenResponse.Token;
                }
                else
                {
                    return String.Empty;
                }
            }
            catch (Exception e)
            {
                Log.LogMessage(string.Format("Error getting token for ProfileMetrics, error = {0}", e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                return String.Empty;
            }
        }
    }
}