﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPMMailerMonitor.Models
{
    public class VelocityAPIData
    {
        private static DateTime LastDirectoriesRefresh;
        private static List<Directory> _DirectoriesData;
        private static int DirectoryDataMaxAge = int.Parse(ConfigurationManager.AppSettings["DirectoryDataMaxAge"]);
        public static List<Directory> DirectoriesData
        {
            get
            {
                if (_DirectoriesData == null || (DateTime.Now - LastDirectoriesRefresh).Seconds > DirectoryDataMaxAge)
                {
                    RefreshData();
                }
                return _DirectoriesData;
            }
        }

        public Status Status { get; set; }
        public List<Directory> Directories { get; set; }

        private static void RefreshData()
        {
            //_DirectoriesData = Get data from API
        }
    }

    public class Status
    {
        public bool Success { get; set; }
        public object Error { get; set; }
    }

    public class Directory
    {
        public int ID { get; set; }
        public List<Profile> Profiles { get; set; }
    }

    public class Profile
    {
        public int ID { get; set; }
        public List<string> ReportEmails { get; set; }
    }
}
