﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TPMLog;

namespace TPMMailerMonitor
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.LogMessage(string.Format("Monitor started"), ConfigurationManager.AppSettings["LogFileName"]);
            try
            {
                var MailerMonitor = new TPMMailerMonitor();
                MailerMonitor.RunMonitor();
            }
            catch (Exception e)
            {
                Log.LogMessage(string.Format("Could not start monitor, error = {0}", e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                return;
            }
            Log.LogMessage(string.Format("Monitor exited"), ConfigurationManager.AppSettings["LogFileName"]);
        }
    }
}