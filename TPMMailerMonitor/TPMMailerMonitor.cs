﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;

using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;

using TPMLog;
using System.IO;
using System.Diagnostics;

namespace TPMMailerMonitor
{
    class TPMMailerMonitor
    {
        private int MaxWorkers;
        private int WorkerCount = 0;
        private string TPMMailerQueue = ConfigurationManager.AppSettings["TPMMailerQueue"];
        public TPMMailerMonitor()
        {
            
        }
        public void RunMonitor()
        {
            try
            {
                MaxWorkers = int.Parse(ConfigurationManager.AppSettings["MaxWorkers"]);
                MonitorQueue();
            }
            catch (Exception e)
            {
                Log.LogMessage(string.Format("Could not start monitor queue, error = {0}", e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                return;
            }
        }

        private void MonitorQueue()
        {
            Log.LogMessage(string.Format("Starting monitor execution"), ConfigurationManager.AppSettings["LogFileName"]);

            while (true)
            {
                var AvailableMessages = GetMessages(1);
                foreach (var Message in AvailableMessages)
                {
                    while (WorkerCount >= MaxWorkers)
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                    var LockObject = new Object();
                    lock (LockObject)
                    {
                        WorkerCount++;
                    }
                    System.Threading.Thread.Sleep(200);
                    string LogMessage = "Launching worker for message ID = " + Message.MessageId;
                    Log.LogMessage(string.Format(LogMessage), ConfigurationManager.AppSettings["LogFileName"]);
                    Console.WriteLine(LogMessage);
                    Task.Factory.StartNew(() => LaunchWorker(Message),
                                                new System.Threading.CancellationToken(),
                                                TaskCreationOptions.None,
                                                TaskScheduler.Default);
                }

                System.Threading.Thread.Sleep(100);
            }
        }

        private void LaunchWorker(Amazon.SQS.Model.Message Message)
        {
            try
            {
                var WorkerArguments = Message.Body.Replace("\"", "\\\"");
                WorkerArguments = WorkerArguments.Replace(";", ",");

                var appLaunchInfo = new System.Diagnostics.ProcessStartInfo()
                {
                    FileName = ConfigurationManager.AppSettings["TPMWorkerExePath"],
                    Arguments = WorkerArguments
                };
                var worker = Process.Start(appLaunchInfo);
                worker.WaitForExit();
                var ExitCode = worker.ExitCode;

                if (ExitCode == 0)
                {
                    Log.LogMessage(string.Format("Finished working for message = {0}", Message.Body), ConfigurationManager.AppSettings["LogFileName"]);
                    DeleteMessage(Message.ReceiptHandle);
                }
                else
                {
                    Log.LogMessage(string.Format("There was an error working for message = {0}", Message.Body), ConfigurationManager.AppSettings["LogFileErrorName"]);
                }
            }
            finally
            {
                var LockObject = new Object();
                lock (LockObject)
                {
                    WorkerCount--;
                }
            }
        }

        private List<Amazon.SQS.Model.Message> GetMessages(int MaxMessages)
        {
            var sqs = new AmazonSQSClient();
            var receiveMessageRequest = new ReceiveMessageRequest { QueueUrl = TPMMailerQueue, MaxNumberOfMessages = MaxMessages };
            var receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);

            return receiveMessageResponse.Messages;
        }

        private void DeleteMessage(string ReceiptHandle)
        {
            var sqs = new AmazonSQSClient();
            var deleteMessageRequest = new DeleteMessageRequest { QueueUrl = TPMMailerQueue, ReceiptHandle = ReceiptHandle };
            sqs.DeleteMessage(deleteMessageRequest);
        }
    }
}
