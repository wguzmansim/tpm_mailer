﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TPMLog;

namespace TPMMailerStarter
{
    class Program
    {
        public static void Main(string[] args)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            try
            {
                TPMMailerStarter Starter = new TPMMailerStarter();
                Starter.SendQueueMessage();
            }
            catch (Exception e)
            {
                Log.LogMessage(string.Format("Could not send queue message, error = {0}", e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
            }

            return;
        }
    }
}
