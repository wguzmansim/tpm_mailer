﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;
using System.Configuration;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using TPMLog;

namespace TPMMailerStarter
{
    public class TPMMailerStarter
    {
        private string VelocityTokenURL = ConfigurationManager.AppSettings["VelocityTokenURL"];
        private string VelocityEmailListURL = ConfigurationManager.AppSettings["VelocityEmailListURL"];
        private string VelocityAPIKey = ConfigurationManager.AppSettings["VelocityAPIKey"];
        private string TPMMailerQueue = ConfigurationManager.AppSettings["TPMMailerQueue"];

        public void SendQueueMessage()
        {
            try
            {
                var sqs = new AmazonSQSClient();
                var EmailList = GetProfileEmailList();

                //Saves profile emails list
                string DatePathFolder = DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") + "\\" + DateTime.Now.ToString("dd");
                System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings["ProfileEmailListFolder"] + "\\" + DatePathFolder);
                using (StreamWriter file = File.CreateText(ConfigurationManager.AppSettings["ProfileEmailListFolder"] + "\\" + DatePathFolder + "\\ProfileEmailList_" + DateTime.Now.ToString("yyyy_MM_dd") + ".json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, EmailList);
                };

                foreach (var Directory in EmailList.Directories)
                {
                    foreach (var Profile in Directory.Profiles)
                    {
                        string MessageBody = "{\"DirectoryID\": \"" + Directory.ID + "\",\"ProfileID\": \"" + Profile.ID + "\",\"Email\": [\"" + String.Join("\",\"", Profile.ReportEmails) + "\"]}";
                        var sendMessageRequest = new SendMessageRequest
                        {
                            QueueUrl = TPMMailerQueue,
                            MessageBody = MessageBody
                        };
                        sqs.SendMessage(sendMessageRequest);
                    }
                }
            }
            catch (Exception e)
            {
                Log.LogMessage(string.Format("Could not send queue message, error = {0}", e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                return;
            }
        }

        private VelocityEmailList GetProfileEmailList()
        {
            try
            {
                //Tries to get a token for API call
                string TokenString = GetToken();
                if (TokenString == "")
                {
                    return new VelocityEmailList();
                }
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(VelocityEmailListURL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                httpWebRequest.Headers["Token"] = TokenString;
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                var VelocityEmailListResponse = JsonConvert.DeserializeObject<VelocityEmailList>(result);

                return VelocityEmailListResponse;

            }
            catch (Exception e)
            {
                Log.LogMessage(string.Format("Could not get profile email list, error = {0}", e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                return new VelocityEmailList();
            }
        }
        private string GetToken()
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(VelocityTokenURL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"ApiKey\":\"" + ConfigurationManager.AppSettings["VelocityAPIKey"] + "\"}";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                var VelocityAPITokenResponse = JsonConvert.DeserializeObject<VelocityToken>(result);
                if (VelocityAPITokenResponse.Success)
                {
                    return VelocityAPITokenResponse.Token;
                }
                else
                {
                    return String.Empty;
                }
            }
            catch (Exception e)
            {
                Log.LogMessage(string.Format("Could not get token for VelocityAPI, error = {0}", e.Message), ConfigurationManager.AppSettings["LogFileErrorName"]);
                return string.Empty;
            }
        }
    }
}
