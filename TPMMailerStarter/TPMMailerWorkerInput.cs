﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPMMailerStarter
{
    class TPMMailerWorkerInput
    {
        public string DirectoryID { get; set; }
        public string ProfileID { get; set; }
        public List<string> Email { get; set; }
    }
}
