﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPMMailerStarter
{
    public class Status
    {
        public bool Success { get; set; }
        public object Error { get; set; }
    }

    public class Profile
    {
        public int ID { get; set; }
        public List<string> ReportEmails { get; set; }
    }

    public class Directory
    {
        public int ID { get; set; }
        public List<Profile> Profiles { get; set; }
    }

    public class VelocityEmailList
    {
        public Status Status { get; set; }
        public List<Directory> Directories { get; set; }
    }
}
