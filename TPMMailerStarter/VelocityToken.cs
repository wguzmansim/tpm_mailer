﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPMMailerStarter
{
    public class VelocityToken
    {
        public bool Success { get; set; }
        public string Token { get; set; }
        public object Error { get; set; }
    }
}
